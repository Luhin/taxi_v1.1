package com.htp.taxi.entity;

import javax.persistence.*;

/**
 * User is an object, which contains all information about user.
 *
 * @author Uladzimir Luhin
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class User extends BaseEntity{
    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userId;
    @Column
    private String login;
    @Column
    private String password;
    @Column
    private String name;
    @Column(name = "e_mail")
    private String eMail;
    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private Rating rating;
    @Column(name = "banned")
    private boolean isBanned;
    @Transient
    private String formattedRating;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public boolean isBanned() {
        return isBanned;
    }

    public void setBanned(boolean banned) {
        isBanned = banned;
    }

    public String getFormattedRating() {
        return formattedRating;
    }

    public void setFormattedRating(String formattedRating) {
        this.formattedRating = formattedRating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (userId != user.userId) return false;
        if (isBanned != user.isBanned) return false;
        if (login != null ? !login.equals(user.login) : user.login != null) return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;
        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        if (eMail != null ? !eMail.equals(user.eMail) : user.eMail != null) return false;
        if (rating != null ? !rating.equals(user.rating) : user.rating != null) return false;
        return formattedRating != null ? formattedRating.equals(user.formattedRating) : user.formattedRating == null;

    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (eMail != null ? eMail.hashCode() : 0);
        result = 31 * result + (rating != null ? rating.hashCode() : 0);
        result = 31 * result + (isBanned ? 1 : 0);
        result = 31 * result + (formattedRating != null ? formattedRating.hashCode() : 0);
        return result;
    }
}