package com.htp.taxi.entity;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Владимир on 24.11.2016.
 */
@Entity
public class Address extends BaseEntity{
    @Id
    private int address_id;
    @Column(name = "street")
    private String streetName;
    @Transient
    private int houseNum;
    @OneToMany(mappedBy = "destinationAddress")
    private Set<Order> ordersDestination;
    @OneToMany(mappedBy = "positionAddress")
    private Set<Order> ordersPosition;

    public int getAddress_id() {
        return address_id;
    }

    public void setAddress_id(int address_id) {
        this.address_id = address_id;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public Set<Order> getOrdersDestination() {
        return ordersDestination;
    }

    public void setOrdersDestination(Set<Order> ordersDestination) {
        this.ordersDestination = ordersDestination;
    }

    public Set<Order> getOrdersPosition() {
        return ordersPosition;
    }

    public void setOrdersPosition(Set<Order> ordersPosition) {
        this.ordersPosition = ordersPosition;
    }

    public int getHouseNum() {
        return houseNum;
    }

    public void setHouseNum(int houseNum) {
        this.houseNum = houseNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;

        Address address = (Address) o;

        if (address_id != address.address_id) return false;
        if (houseNum != address.houseNum) return false;
        if (streetName != null ? !streetName.equals(address.streetName) : address.streetName != null) return false;
        if (ordersDestination != null ? !ordersDestination.equals(address.ordersDestination) : address.ordersDestination != null)
            return false;
        return ordersPosition != null ? ordersPosition.equals(address.ordersPosition) : address.ordersPosition == null;

    }

    @Override
    public int hashCode() {
        int result = address_id;
        result = 31 * result + (streetName != null ? streetName.hashCode() : 0);
        result = 31 * result + houseNum;
        result = 31 * result + (ordersDestination != null ? ordersDestination.hashCode() : 0);
        result = 31 * result + (ordersPosition != null ? ordersPosition.hashCode() : 0);
        return result;
    }
}
