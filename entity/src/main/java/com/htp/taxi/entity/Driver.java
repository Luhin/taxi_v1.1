package com.htp.taxi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import java.util.Set;

/**
 * Created by Владимир on 24.11.2016.
 */
@Entity
@PrimaryKeyJoinColumn(name = "driver_id")
public class Driver extends User {
    @Column(name = "car_model")
    private String carModel;
    @OneToMany(mappedBy = "driver")
    private Set<Order> orders;

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Driver)) return false;
        if (!super.equals(o)) return false;

        Driver driver = (Driver) o;

        if (carModel != null ? !carModel.equals(driver.carModel) : driver.carModel != null) return false;
        return orders != null ? orders.equals(driver.orders) : driver.orders == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (carModel != null ? carModel.hashCode() : 0);
        result = 31 * result + (orders != null ? orders.hashCode() : 0);
        return result;
    }
}
