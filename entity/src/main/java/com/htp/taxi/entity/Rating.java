package com.htp.taxi.entity;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;

/**
 * Created by Владимир on 24.11.2016.
 */
@Entity
public class Rating extends BaseEntity {
    @Id
    @GenericGenerator(name = "gen", strategy = "foreign", parameters = @org.hibernate.annotations.Parameter(name = "property", value = "user"))
    @GeneratedValue(generator = "gen")
    @Column(name = "user_id")
    private int rating_id;
    @Column(name = "mark_sum")
    private int markSum;
    @Column(name = "mark_count")
    private int markCount;
    @OneToOne
    @PrimaryKeyJoinColumn
    private User user;

    public double calcRating() {
        return (double) markSum / (double) markCount;
    }

    public int getRating_id() {
        return rating_id;
    }

    public void setRating_id(int rating_id) {
        this.rating_id = rating_id;
    }

    public int getMarkSum() {
        return markSum;
    }

    public void setMarkSum(int markSum) {
        this.markSum = markSum;
    }

    public int getMarkCount() {
        return markCount;
    }

    public void setMarkCount(int markCount) {
        this.markCount = markCount;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
