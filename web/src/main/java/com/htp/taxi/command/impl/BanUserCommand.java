package com.htp.taxi.command.impl;

import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.entity.User;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

public class BanUserCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        int userId = Integer.parseInt(request.getParameter(AttributeName.USER_ID));
        ArrayList<User> userList = (ArrayList<User>) session.getAttribute(AttributeName.USER_LIST);
        try {
            UserService.getInstance().banUser(userId, true, userList);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        session.setAttribute(AttributeName.USER_LIST, userList);
        String page = (String) session.getAttribute(AttributeName.LAST_PAGE);
        return page;
    }
}
