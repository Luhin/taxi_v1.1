package com.htp.taxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.PageName;
import com.htp.taxi.exception.CommandException;

public class LocalizationCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        String locale = request.getParameter(AttributeName.LOCALE);
        session.setAttribute(AttributeName.LOCALE, locale);
        String lastPage = (String)session.getAttribute(AttributeName.LAST_PAGE);
        if (lastPage == null) {
            return PageName.AUTHORIZATION;
        } else {
            return lastPage;
        }
    }
}
