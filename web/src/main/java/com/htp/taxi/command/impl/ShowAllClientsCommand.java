package com.htp.taxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.PageName;
import com.htp.taxi.entity.Client;
import com.htp.taxi.entity.User;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.ClientService;

public class ShowAllClientsCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        List<Client> clientsList;
        try {
            clientsList = ClientService.getInstance().findAllClients();
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        session.setAttribute(AttributeName.USER_LIST, clientsList);
        session.setAttribute(AttributeName.LAST_PAGE, PageName.SHOW_ALL_CLIENTS);
        return PageName.SHOW_ALL_CLIENTS;
    }
}
