package com.htp.taxi.command.impl;

import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.PageName;
import com.htp.taxi.entity.Driver;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.DriverService;
import com.htp.taxi.validation.Validation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ChangeDriverCarCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        Driver driver = (Driver) session.getAttribute(AttributeName.CLIENT);
        String newCar = request.getParameter(AttributeName.NEW_CAR);
        String password = request.getParameter(AttributeName.PASSWORD);
        if (!Validation.getInstance().validatePassword(password)) {
            request.setAttribute(AttributeName.INVALID_DATA, true);
            return PageName.USER_PAGE;
        }
        try {
            if (DriverService.getInstance().changeDriverCar(driver, newCar, password)) {
                request.setAttribute(AttributeName.SUCCESS_OPERATION, true);
            } else {
                request.setAttribute(AttributeName.INVALID_DATA, true);
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        session.setAttribute(AttributeName.LAST_PAGE, PageName.DRIVER_PROFILE);
        return PageName.DRIVER_PROFILE;
    }
}
