package com.htp.taxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.PageName;
import com.htp.taxi.entity.Client;
import com.htp.taxi.entity.User;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.OrderService;

public class WaitOrderAcceptanceCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute(AttributeName.CLIENT);
        String answer;
        try {
            answer = OrderService.getInstance().waitOrderAcceptance(user.getUserId());
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        try {
            response.getWriter().write(answer);
        } catch (IOException e1) {
            throw new CommandException(e1);
        }
        return PageName.AJAX;
    }
}