package com.htp.taxi.command;

public class PageName {
    public final static String USER_PAGE = "jsp/user_page.jsp";
    public final static String AUTHORIZATION = "jsp/authorization.jsp";
    public final static String ERROR_PAGE = "jsp/error/error_page.jsp";
    public final static String CREATE_ORDER = "jsp/create_order.jsp";
    public final static String CONFIRM_ORDER = "jsp/confirm_order.jsp";
    public final static String ORDER_LIST = "jsp/order_list.jsp";
    public final static String RESULT_ORDER = "jsp/result_order.jsp";
    public final static String AJAX = "ajax";
    public final static String CLIENT_PROFILE = "jsp/client_profile.jsp";
    public final static String USER_ORDER = "jsp/user_orders.jsp";
    public final static String EDIT_COMMENT = "jsp/edit_comment.jsp";
    public final static String DRIVER_PROFILE = "jsp/driver_profile.jsp";
    public final static String RATE_ORDER = "jsp/rate_order.jsp";
    public final static String SHOW_ALL_DRIVERS = "jsp/show_all_drivers.jsp";
    public final static String SHOW_ALL_CLIENTS = "jsp/show_all_clients.jsp";

    private PageName() {
    }
}
