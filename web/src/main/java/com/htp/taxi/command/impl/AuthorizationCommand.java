package com.htp.taxi.command.impl;

import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.PageName;
import com.htp.taxi.entity.Admin;
import com.htp.taxi.entity.Client;
import com.htp.taxi.entity.Driver;
import com.htp.taxi.entity.User;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.UserService;
import com.htp.taxi.validation.Validation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AuthorizationCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String login = request.getParameter(AttributeName.LOGIN);
        String password = request.getParameter(AttributeName.PASSWORD);
        HttpSession session = request.getSession();
        if (!validateData(login, password)) {
            request.setAttribute(AttributeName.INVALID_DATA, true);
            return PageName.AUTHORIZATION;
        }
        try {
            User user = UserService.getInstance().authorizeUser(login, password);
            if (user != null) {
                session.setAttribute(AttributeName.CLIENT, user);
                session.setAttribute(AttributeName.LAST_PAGE, PageName.USER_PAGE);
                return PageName.USER_PAGE;
            } else {
                request.setAttribute(AttributeName.INVALID_DATA, true);
                return PageName.AUTHORIZATION;
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
    }

    private boolean validateData(String login, String password) {
        Validation validation = Validation.getInstance();
        return validation.validateLogin(login) && validation.validatePassword(password);
    }
}
