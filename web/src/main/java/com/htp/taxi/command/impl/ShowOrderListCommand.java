package com.htp.taxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.PageName;
import com.htp.taxi.entity.Order;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.impl.OrderService;

public class ShowOrderListCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        ArrayList<Order> orderList = OrderService.getInstance().showOrders();
        HttpSession session = request.getSession();
        session.setAttribute(AttributeName.ORDER_LIST, orderList);
        session.setAttribute(AttributeName.LAST_PAGE, PageName.ORDER_LIST);
        return PageName.ORDER_LIST;
    }
}
