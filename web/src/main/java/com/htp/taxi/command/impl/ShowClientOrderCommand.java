package com.htp.taxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.PageName;
import com.htp.taxi.entity.Client;
import com.htp.taxi.entity.Order;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.OrderService;
import org.w3c.dom.Attr;

public class ShowClientOrderCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        Client client = (Client)session.getAttribute(AttributeName.CLIENT);
        ArrayList<Order> orderList;
        try {
            orderList = OrderService.getInstance().findUserOrder(client);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        session.setAttribute(AttributeName.ORDER_LIST, orderList);
        session.setAttribute(AttributeName.LAST_PAGE, PageName.USER_ORDER);
        return PageName.USER_ORDER;
    }
}
