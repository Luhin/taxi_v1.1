package com.htp.taxi.filter;

import com.htp.taxi.dao.HIbernateSessionManager;
import org.hibernate.Session;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Владимир on 28.11.2016.
 */
@WebFilter(urlPatterns = {"/*"})
public class HibernateFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
        Session session = HIbernateSessionManager.getInstance().getSessionFactory().openSession();
        filterChain.doFilter(httpRequest, httpResponse);
        session.close();

    }

    @Override
    public void destroy() {
    }
}