<fmt:message var="showProfile" bundle="${loc}" key="locale.show.profile" />
<fmt:message var="orderList" bundle="${loc}"
	key="locale.order.list" />
<div>
	<form action="Controller" method="post">
		<input type="hidden" name="command" value="show_order_list" /> <input class="btn btn-primary"
			type="submit" value="${orderList }" />
	</form>
	<form action="Controller" method="post">
		<input type="hidden" name="command" value="show_driver_profile" /> <input class="btn btn-info"
			type="submit" value="${showProfile }" />
	</form>
</div>