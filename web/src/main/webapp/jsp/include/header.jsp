<fmt:message var="choseLanguage" bundle="${loc}"
	key="locale.settings.choselanguage" />
<fmt:message var="logout" bundle="${loc}" key="locale.button.logout" />
<fmt:message var="ruButton" bundle="${loc}"
	key="locale.locbutton.name.ru" />
<fmt:message var="enButton" bundle="${loc}"
	key="locale.locbutton.name.en" />
<fmt:message var="toMainPage" bundle="${loc}"
	key="locale.button.to.main.page" />
<div class="top">
	<div class="logo">
		<h1>ITAXI</h1>
		<form action="Controller" method="post">
				<input type="hidden" name="command" value="to-main-page" /> <input class="btn"
					type="submit" value="${toMainPage }" />
			</form>
	</div>
	<div class="personal">
		<c:if test="${sessionScope.client != null}">
			<form action="Controller" method="post">
				<input type="hidden" name="command" value="logout" /> <input class="btn"
					type="submit" value="${logout }" />
			</form>
		</c:if>
	</div>
	<br /><br>
	<div class="localization">
		${choseLanguage }
		<form action="Controller" method="post">
			<input type="hidden" name="locale" value="ru" /> <input type="hidden"
				name="command" value="localization" /> <input class="btn btn-info" type="submit"
				value="${ruButton}" />
		</form>
		<form action="Controller" method="post">
			<input type="hidden" name="locale" value="en" /> <input type="hidden"
				name="command" value="localization" /> <input class="btn btn-info" type="submit"
				value="${enButton}" />
		</form>
	</div>
</div>
<hr>