<fmt:message var="copyrights" bundle="${loc}"
	key="locale.footer.copyright" />
<div class="footer">
	<hr>
	<div class="copyright">${copyrights }</div>
</div>
