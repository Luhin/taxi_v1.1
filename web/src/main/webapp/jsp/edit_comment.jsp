<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="custom-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle var="loc" basename="localization.locale" />
<fmt:message var="enterNewComment" bundle="${loc}" key="locale.message.enter.new.comment" />
<fmt:message var="orderEditingTitle" bundle="${loc}" key="locale.title.comment.editing" />
<fmt:message var="ready" bundle="${loc}" key="locale.button.ready" />
<fmt:message var="commentInfo" bundle="${loc}" key="locale.message.comment.info" />
<title>${orderEditingTitle }</title>
</head>
<body>
	<%@ include file="include/header.jsp"%>
	<div class="content">
	${enterNewComment }
	<br>
	${commentInfo }
	<br>
	<c:if test="${not empty requestScope.invalidRegistrData}">
			<span class="error">${requestScope.invalidRegistrData }</span>
			<br>
			</c:if>
	<form action="Controller" method="post">
		<input type="hidden" name="command" value="edit-comment" />
		<input type="hidden" name="pageUnique" value="${sessionScope.pageUnique }"/>
		<textarea name="newComment" value="" required pattern="[a-zA-Zа-яёА-ЯЁ\\s0-9!,.?]{1,250}" cols="40" rows="4"></textarea> 
		<br>
		<input class='btn btn-primary' type="submit" value="${ready }" />
	</form>
	</div>
	<%@ include file="include/footer.jsp"%>
</body>
</html>