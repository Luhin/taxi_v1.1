<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle var="loc" basename="localization.locale" />
<fmt:message var="hello1" bundle="${loc}" key="locale.hello.client1" />
<fmt:message var="userTitle" bundle="${loc}" key="locale.title.user" />
<fmt:message var="orderCompleted" bundle="${loc}"
	key="locale.message.completed.order" />
<title>${userTitle }</title>
</head>
<body>
	<%@ include file="include/header.jsp"%>
	<div class="content">
		${hello1 }
		<c:out value="${sessionScope.client.name}" />
		<c:if test="${not empty requestScope.successOrder}">
			<br />
			<h1>${orderCompleted }</h1>
		</c:if>
		<c:if test="${not empty requestScope.errorMessage}">
			<br />
			<span class="error">${errorMessage }</span>
		</c:if>
		<br />
		<c:if test="${sessionScope.client.getClass().getSimpleName() eq 'Admin'}">
			<%@ include file="include/admin_part.jsp"%>
		</c:if>
		<c:if test="${sessionScope.client.getClass().getSimpleName() eq 'Driver'}">
			<%@ include file="include/driver_part.jsp"%>
		</c:if>
		<c:if test="${sessionScope.client.getClass().getSimpleName() eq 'Client'}">
			<%@ include file="include/client_part.jsp"%>
		</c:if>
	</div>
	<%@ include file="include/footer.jsp"%>
</body>
</html>