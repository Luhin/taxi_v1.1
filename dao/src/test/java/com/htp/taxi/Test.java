package com.htp.taxi;


import com.htp.taxi.dao.DAOFactory;
import com.htp.taxi.dao.exception.DAOException;
import com.htp.taxi.dao.impl.AddressDAO;
import com.htp.taxi.dao.impl.OrderDAO;
import com.htp.taxi.dao.impl.UserDAO;
import com.htp.taxi.entity.*;
import org.junit.Assert;
import org.junit.Before;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Владимир on 24.11.2016.
 */
public class Test {
    private OrderDAO orderDAO;
    private UserDAO userDAO;

    @Before
    public void init() {
        orderDAO = DAOFactory.getInstance().getOrderDAO();
        userDAO = DAOFactory.getInstance().getUserDAO();
    }

    @org.junit.Test
    public void findAll() throws DAOException {
        OrderDAO orderDAO = DAOFactory.getInstance().getOrderDAO();
        Driver driver = new Driver();
        driver.setUserId(11);
        List<Order> list = orderDAO.findUserOrders(driver);
        System.out.println(list.size());
    }

    @org.junit.Test
    public void checkExistLogin() throws DAOException {
        UserDAO userDAO = DAOFactory.getInstance().getUserDAO();
        Assert.assertFalse(userDAO.checkExistLogin("admidfgn"));
    }

    @org.junit.Test
    public void randomAddress() throws DAOException {
        AddressDAO addressDAO = DAOFactory.getInstance().getAddressDAO();
        System.out.println(addressDAO.determinePosition().getStreetName());
    }


    @org.junit.Test
    public void banUser() throws DAOException {
        userDAO.banUser(2, false);
    }
    @org.junit.Test
    public void findStreet() throws DAOException {
        Address address = DAOFactory.getInstance().getAddressDAO().findAddress("Лещинского");
        System.out.println(address.getAddress_id());
    }

}
