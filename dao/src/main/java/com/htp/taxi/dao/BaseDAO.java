package com.htp.taxi.dao;

import com.htp.taxi.dao.exception.DAOException;
import com.htp.taxi.entity.BaseEntity;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.List;

/**
 * Created by Владимир on 24.11.2016.
 */
public abstract class BaseDAO<T extends BaseEntity> {
    public abstract Class getPersistentClass();

    public int save(T t) throws DAOException {
        Transaction transaction = null;
        try {
            Session session = HIbernateSessionManager.getInstance().currentSession();
            transaction = session.beginTransaction();
            int result = (Integer) session.save(t);
            transaction.commit();
            return result;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new DAOException(e);
        }


    }

    public T find(int id) throws DAOException {
        try {
            Session session = HIbernateSessionManager.getInstance().currentSession();
            T t = (T) session.get(getPersistentClass(), id);
            return t;
        } catch (HibernateException e) {
            throw new DAOException(e);
        }
    }

    public List<T> findAll() throws DAOException {
        try {
            Session session = HIbernateSessionManager.getInstance().currentSession();
            Criteria criteria = session.createCriteria(getPersistentClass());
            List<T> list = criteria.list();
            return criteria.list();
        } catch (HibernateException e) {
            throw new DAOException(e);
        }
    }

    public T merge(T t) throws DAOException {
        Transaction transaction = null;
        try {
            Session session = HIbernateSessionManager.getInstance().currentSession();
            transaction = session.beginTransaction();
            session.merge(t);
            transaction.commit();
            return t;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new DAOException(e);
        }
    }
}
