package com.htp.taxi.dao;

import com.htp.taxi.dao.exception.DAOException;
import com.htp.taxi.entity.User;

/**
 * Created by Владимир on 28.11.2016.
 */
public interface IUserDAO {
    User authorizeUser(String login, String password) throws DAOException;

    boolean checkExistLogin(String login) throws DAOException;

    int rateUser(User user, int rating) throws DAOException;

    void banUser(int userId, boolean ban) throws DAOException;

}
