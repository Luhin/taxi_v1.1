package com.htp.taxi.dao.impl;

import com.htp.taxi.dao.BaseDAO;
import com.htp.taxi.dao.HIbernateSessionManager;
import com.htp.taxi.dao.IUserDAO;
import com.htp.taxi.dao.exception.DAOException;
import com.htp.taxi.entity.Rating;
import com.htp.taxi.entity.User;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;


/**
 * Created by Владимир on 28.11.2016.
 */
public class UserDAO extends BaseDAO<User> implements IUserDAO {
    private final static String LOGIN_COLUMN = "login";
    private final static String PASSWORD_COLUMN = "password";

    public User authorizeUser(String login, String password) throws DAOException {
        try {
            User user = null;
            Session session = HIbernateSessionManager.getInstance().currentSession();
            Criteria criteria = session.createCriteria(getPersistentClass());
            criteria.add(Restrictions.eq(LOGIN_COLUMN, login));
            criteria.add(Restrictions.eq(PASSWORD_COLUMN, password));
            Object result = criteria.uniqueResult();
            if (result != null) {
                user = (User) result;
            }
            return user;
        } catch (HibernateException e) {
            throw new DAOException(e);
        }
    }

    public boolean checkExistLogin(String login) throws DAOException {
        try {
            Session session = HIbernateSessionManager.getInstance().currentSession();
            Criteria criteria = session.createCriteria(getPersistentClass());
            criteria.add(Restrictions.eq(LOGIN_COLUMN, login));
            criteria.setProjection(Projections.property(LOGIN_COLUMN));
            Object object = criteria.uniqueResult();
            return object != null;
        } catch (HibernateException e) {
            throw new DAOException(e);
        }
    }

    public int rateUser(User user, int rating) throws DAOException {
        Transaction transaction = null;
        try {
            Session session = HIbernateSessionManager.getInstance().currentSession();
            transaction = session.beginTransaction();
            Rating userRating = user.getRating();
            userRating.setMarkCount(userRating.getMarkCount() + 1);
            userRating.setMarkSum(userRating.getMarkSum() + rating);
            transaction.commit();
            return 1;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new DAOException(e);
        }
    }

    @Override
    public void banUser(int userId, boolean ban) throws DAOException {
        Transaction transaction = null;
        try {
            Session session = HIbernateSessionManager.getInstance().currentSession();
            transaction = session.beginTransaction();
            User user = (User) session.get(getPersistentClass(), userId);
            user.setBanned(ban);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new DAOException(e);
        }
    }

    public Class getPersistentClass() {
        return User.class;
    }
}
