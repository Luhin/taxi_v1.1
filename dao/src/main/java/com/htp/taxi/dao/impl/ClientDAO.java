package com.htp.taxi.dao.impl;

import com.htp.taxi.dao.BaseDAO;
import com.htp.taxi.dao.HIbernateSessionManager;
import com.htp.taxi.dao.IClientDAO;
import com.htp.taxi.dao.exception.DAOException;
import com.htp.taxi.entity.BaseEntity;
import com.htp.taxi.entity.Client;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;


/**
 * Created by Владимир on 28.11.2016.
 */
public class ClientDAO extends BaseDAO<Client> implements IClientDAO {
    private final static String CLIENT_ID_COLUMN = "userId";
    private final static String CARD_NUMBER_COLUMN = "cardNumber";

    @Override
    public boolean checkCardNumber(Client client, int cardNumber) throws DAOException {
        Transaction transaction = null;
        try {
            Session session = HIbernateSessionManager.getInstance().currentSession();
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(getPersistentClass());
            criteria.add(Restrictions.eq(CLIENT_ID_COLUMN, client.getUserId()));
            criteria.setProjection(Projections.property(CARD_NUMBER_COLUMN));
            int realCardNumber = (Integer) criteria.uniqueResult();
            transaction.commit();
            return realCardNumber == cardNumber;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new DAOException(e);
        }
    }

    @Override
    public Class getPersistentClass() {
        return Client.class;
    }
}
