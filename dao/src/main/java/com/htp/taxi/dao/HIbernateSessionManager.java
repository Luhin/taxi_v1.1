package com.htp.taxi.dao;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by Владимир on 24.11.2016.
 */
public class HIbernateSessionManager {
    private static Logger logger = LogManager.getLogger();
    private SessionFactory sessionFactory;
    private static final ThreadLocal local = new ThreadLocal();

    private HIbernateSessionManager() {
        try {
            sessionFactory = new Configuration().configure().buildSessionFactory();
        } catch (HibernateException e) {
            logger.fatal("HIbernate session factory initialization error", e);
            throw new RuntimeException();
        }
    }

    private static class SingletonHolder {
        private static final HIbernateSessionManager INSTANCE = new HIbernateSessionManager();
    }

    public static HIbernateSessionManager getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public SessionFactory getSessionFactory(){
        return sessionFactory;
    }
    public void shutdown() {
        sessionFactory.close();
    }

    public Session currentSession() {
        Session session = (Session) local.get();
        if (session == null) {
            session = sessionFactory.openSession();
            local.set(session);
        }
        return session;
    }
}
