package com.htp.taxi.dao.impl;

import com.htp.taxi.dao.BaseDAO;
import com.htp.taxi.dao.HIbernateSessionManager;
import com.htp.taxi.dao.IAddressDAO;
import com.htp.taxi.dao.exception.DAOException;
import com.htp.taxi.entity.Address;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * Created by Владимир on 29.11.2016.
 */
public class AddressDAO extends BaseDAO<Address> implements IAddressDAO {
    public static final String STREET_COLUMN = "streetName";

    @Override
    public Address determinePosition() throws DAOException {
        try {
            Session session = HIbernateSessionManager.getInstance().currentSession();
            Criteria criteria = session.createCriteria(getPersistentClass());
            criteria.add(Restrictions.sqlRestriction("1=1 order by rand()"));
            criteria.setMaxResults(1);
            Address address = (Address) criteria.uniqueResult();
            return address;
        } catch (HibernateException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public Address findAddress(String streetName) throws DAOException {
        try {
            Address address = null;
            Session session = HIbernateSessionManager.getInstance().currentSession();
            Criteria criteria = session.createCriteria(getPersistentClass());
            criteria.add(Restrictions.eq(STREET_COLUMN, streetName));
            Object result = criteria.uniqueResult();
            if (result != null) {
                address = (Address) result;
            }
            return address;
        } catch (HibernateException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public Class getPersistentClass() {
        return Address.class;
    }
}
