package com.htp.taxi.dao.impl;

import com.htp.taxi.dao.BaseDAO;
import com.htp.taxi.dao.HIbernateSessionManager;
import com.htp.taxi.dao.IOrderDAO;
import com.htp.taxi.dao.exception.DAOException;
import com.htp.taxi.entity.Client;
import com.htp.taxi.entity.Driver;
import com.htp.taxi.entity.Order;
import com.htp.taxi.entity.User;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.util.List;


/**
 * Created by Владимир on 28.11.2016.
 */
public class OrderDAO extends BaseDAO<Order> implements IOrderDAO {
    private final static String DRIVER_ID_COLUMN = "driver";
    private final static String CLIENT_ID_COLUMN = "client";
    private final static String ORDER_ID_COLUMN = "orderId";

    @Override
    public List<Order> findUserOrders(User user) throws DAOException {
        try {
            Session session = HIbernateSessionManager.getInstance().currentSession();
            Criteria criteria = session.createCriteria(Order.class);
            if (user instanceof Client) {
                criteria.add(Restrictions.eq(CLIENT_ID_COLUMN, user));
            }
            if (user instanceof Driver) {
                criteria.add(Restrictions.eq(DRIVER_ID_COLUMN, user));
            }
            List<Order> list = criteria.list();
            return list;
        } catch (HibernateException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void editUserComment(User user, int orderId, String comment) throws DAOException {
        Transaction transaction = null;
        try {
            Session session = HIbernateSessionManager.getInstance().currentSession();
            transaction = session.beginTransaction();
            Order order = (Order) session.get(getPersistentClass(), orderId);
            if (user instanceof Client) {
                order.setClientComment(comment);
            }
            if (user instanceof Driver) {
                order.setDriverComment(comment);
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new DAOException(e);
        }
    }

    @Override
    public Order findLastOrder(User user) throws DAOException {
        Transaction transaction = null;
        try {
            Session session = HIbernateSessionManager.getInstance().currentSession();
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(getPersistentClass());
            if (user instanceof Driver) {
                criteria.add(Restrictions.eq(DRIVER_ID_COLUMN, user));
            } else {
                criteria.add(Restrictions.eq(CLIENT_ID_COLUMN, user));
            }
            criteria.addOrder(org.hibernate.criterion.Order.desc(ORDER_ID_COLUMN));
            criteria.setMaxResults(1);
            Order order = (Order) criteria.uniqueResult();
            transaction.commit();
            return order;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new DAOException(e);
        }
    }

    @Override
    public void orderCompleted(Order order) throws DAOException {
        Transaction transaction = null;
        try {
            Session session = HIbernateSessionManager.getInstance().currentSession();
            transaction = session.beginTransaction();
            Client client = order.getClient();
            client.setMoneyAmount(client.getMoneyAmount().subtract(order.getPrice()));
            session.save(order);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new DAOException(e);
        }
    }

    @Override
    public Class getPersistentClass() {
        return Order.class;
    }
}
