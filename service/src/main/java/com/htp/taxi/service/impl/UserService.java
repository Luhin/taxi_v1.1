package com.htp.taxi.service.impl;

import com.htp.taxi.dao.DAOFactory;
import com.htp.taxi.dao.exception.DAOException;
import com.htp.taxi.dao.impl.UserDAO;
import com.htp.taxi.entity.Client;
import com.htp.taxi.entity.User;
import com.htp.taxi.service.IUserService;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.util.Constants;
import com.htp.taxi.service.util.DataFormatter;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.List;

public class UserService implements IUserService {
    private final static UserService INSTANCE = new UserService();

    private UserService() {

    }

    public static UserService getInstance() {
        return INSTANCE;
    }

    public User authorizeUser(String login, String password) throws ServiceException {
        User user = null;
        try {
            String encryptedPassword = encryptPassword(password);
            UserDAO userDAO = DAOFactory.getInstance().getUserDAO();
            user = userDAO.authorizeUser(login, encryptedPassword);
            String formattedRating = DataFormatter.formatRating(user.getRating().calcRating());
            user.setFormattedRating(formattedRating);
            return user;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public String encryptPassword(String password) throws ServiceException {
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance(Constants.HASH_FUNCTION);
            messageDigest.update(password.getBytes(), 0, password.length());
            return new BigInteger(messageDigest.digest()).toString();
        } catch (NoSuchAlgorithmException e) {
            throw new ServiceException("Wrong encrypt algorithm", e);
        }
    }

    public boolean matchExistLogin(String login) throws ServiceException {
        try {
            UserDAO userDAO = DAOFactory.getInstance().getUserDAO();
            return userDAO.checkExistLogin(login);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void banUser(int userId, boolean ban, List<User> userList) throws ServiceException {
        try {
            UserDAO userDAO = DAOFactory.getInstance().getUserDAO();
            userDAO.banUser(userId, ban);
            userList.stream().filter(u -> u.getUserId() == userId).forEach(u -> u.setBanned(ban));
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public User updateClientData(int clientId) throws ServiceException {
        try {
            UserDAO userDAO = DAOFactory.getInstance().getUserDAO();
            User user = (User) userDAO.find(clientId);
            String formattedRating = DataFormatter.formatRating(user.getRating().calcRating());
            user.setFormattedRating(formattedRating);
            return user;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
