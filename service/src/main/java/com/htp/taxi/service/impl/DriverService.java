package com.htp.taxi.service.impl;

import com.htp.taxi.dao.DAOFactory;
import com.htp.taxi.dao.exception.DAOException;
import com.htp.taxi.dao.impl.DriverDAO;
import com.htp.taxi.dao.impl.UserDAO;
import com.htp.taxi.entity.Driver;
import com.htp.taxi.entity.Rating;
import com.htp.taxi.entity.User;
import com.htp.taxi.service.IDriverService;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.util.Constants;
import com.htp.taxi.service.util.DataFormatter;

import java.util.ArrayList;
import java.util.List;

public class DriverService implements IDriverService {
    private final static DriverService INSTANCE = new DriverService();

    private DriverService() {

    }

    public static DriverService getInstance() {
        return INSTANCE;
    }

    public boolean changeDriverCar(Driver driver, String newCar, String password) throws ServiceException {
        try {
            String encryptedPassword = UserService.getInstance().encryptPassword(password);
            DriverDAO driverDAO = DAOFactory.getInstance().getDriverDAO();
            Driver persistDriver = driverDAO.find(driver.getUserId());
            if (persistDriver.getPassword().equals(encryptedPassword)) {
                driver.setCarModel(newCar);
                driverDAO.merge(driver);
                return true;
            } else {
                return false;
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void signUpDriver(String name, String login, String password, String email, String carModel)
            throws ServiceException {
        try {
            Driver driver = new Driver();
            driver.setName(name);
            driver.setLogin(login);
            driver.setPassword(UserService.getInstance().encryptPassword(password));
            driver.seteMail(email);
            driver.setCarModel(carModel);
            Rating rating = new Rating();
            rating.setMarkCount(1);
            rating.setMarkSum(Constants.START_RATING);
            driver.setRating(rating);
            rating.setUser(driver);
            DriverDAO driverDAO = DAOFactory.getInstance().getDriverDAO();
            driverDAO.save(driver);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<Driver> findAllDrivers() throws ServiceException {
        List<Driver> driverList;
        try {
            DriverDAO driverDAO = DAOFactory.getInstance().getDriverDAO();
            driverList = driverDAO.findAll();
            driverList.stream().forEach(driver -> {
                String formattedRating = DataFormatter.formatRating(driver.getRating().calcRating());
                driver.setFormattedRating(formattedRating);
            });
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return driverList;
    }
}
