package com.htp.taxi.service;

import com.htp.taxi.entity.Client;
import com.htp.taxi.entity.Driver;
import com.htp.taxi.entity.Order;
import com.htp.taxi.entity.User;
import com.htp.taxi.service.exception.ServiceException;

import java.math.BigDecimal;
import java.util.List;

/**
 * Makes some service actions, relative to the order, and then, if necessary,
 * calls corresponding DAO
 *
 * @author Uladzimir Luhin
 */
public interface IOrderService {
    /**
     * Creates a list, contains possible addresse to go.
     *
     * @return list, contains streets name
     * @throws ServiceException
     */
    List<String> makeStreetList() throws ServiceException;

    /**
     * Creates a new order in the system. New order will be with status new.
     *
     * @param clientId           ID of a client will be the key to order
     * @param currentStreet      curren street, where client is
     * @param currentHouseNumber house number, where client is
     * @param destination        the street, where clietn would like to go
     * @param destHouseNumber    house number
     * @return true, if order created, false if not
     * @throws ServiceException
     */
    boolean createOrder(Client client, String currentStreet, int currentHouseNumber, String destination,
                        String destHouseNumber) throws ServiceException;

    /**
     * Shows all new active orders in the system on the current moment.
     *
     * @return list with all active new orders
     */
    List<Order> showOrders();

    /**
     * Driver accepts one of the new orders, and waiting answer of the client.
     *
     * @param clientId key to the order
     * @return
     * @throws ServiceException
     */
    boolean acceptOrder(int clientId, Driver driver) throws ServiceException;

    /**
     * This method checks the status of the order, and waititng client
     * confirmation
     *
     * @param orderId ID of the order, the same as ID of the client
     * @return String contains answer of the client
     * @throws ServiceException
     */
    String checkOrderResult(int orderId) throws ServiceException;

    /**
     * When client created the new order, he is waiting answer from one of the
     * drivers.
     *
     * @param clientId
     * @return String contains answer of the driver
     * @throws ServiceException
     */
    String waitOrderAcceptance(int clientId) throws ServiceException;

    /**
     * When driver accepts the new order, client looks information about driver
     * and price, and decides go, or not to go
     *
     * @param orderId
     * @return
     * @throws ServiceException
     */
    boolean confirmOrder(int orderId) throws ServiceException;

    /**
     * When driver has accepted the order, this method will be collect all
     * information about driver and sends it to the client
     *
     * @param order
     * @return String with all information about driver and price
     */
    String createAnswerOrder(Order order);

    /**
     * Find total number of orders, made by client.
     *
     * @return list with all client orders
     * @throws ServiceException
     */
    List<Order> findUserOrder(User user) throws ServiceException;

    /**
     * If order was done, or cancelled, this method will be delete ordee from
     * the system.
     *
     * @param userId
     * @throws ServiceException
     */
    void deleteOrder(int userId) throws ServiceException;

    /**
     * Edit a comment to the trip.
     *
     * @param orderId    ID of an order, which comment should be edited
     * @param newComment new comment, which must be addet to the order
     * @throws ServiceException
     */
    void editUserComment(User user, int orderId, String newComment) throws ServiceException;

    /**
     * After successs order system will ask client and driver rate each other
     * and create comment about trip.
     * <p>
     * right place
     *
     * @param comment new comment
     * @param rating  mark of the trip
     * @throws ServiceException
     */
    void rateOrder(User user, String comment, String rating) throws ServiceException;
}
